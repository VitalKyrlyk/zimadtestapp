package com.e.zimadtestapp.utils;

public class Constants {

    public static final String BASE_URL = "http://kot3.com/xim/";
    public static final String KEY_TITLE = "title";
    public static final String KEY_URL = "url";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_POSITION = "position";
    public static final String KEY_LIST = "list";
    public static final String TAG_PREVIOUS_FRAGMENT = "previous_fragment";
    public static final String TAG_CAT_FRAGMENT = "cat_fragment";
    public static final String TAG_DOG_FRAGMENT = "dog_fragment";
    public static final String TAG_DETAIL_FRAGMENT = "detail_fragment";

}
