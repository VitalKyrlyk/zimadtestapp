package com.e.zimadtestapp.fragments.cat;

import android.annotation.SuppressLint;

import com.e.zimadtestapp.core.BasePresenter;
import com.e.zimadtestapp.network.service.NetworkService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CatFragmentPresenter extends BasePresenter<ICatFragment> implements ICatFragmentPresenter {

    private NetworkService networkService;

    public CatFragmentPresenter(ICatFragment view, NetworkService networkService) {
        super(view);
        this.networkService = networkService;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onResumePresenter() {
        networkService.getCats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> view.updatePetsList(response.getPetsList()));
    }

}
