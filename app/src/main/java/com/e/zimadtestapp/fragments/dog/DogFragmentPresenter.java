package com.e.zimadtestapp.fragments.dog;

import android.annotation.SuppressLint;

import com.e.zimadtestapp.core.BasePresenter;
import com.e.zimadtestapp.network.service.NetworkService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DogFragmentPresenter extends BasePresenter<IDogFragment> implements IDogFragmentPresenter {

    private NetworkService networkService;

    public DogFragmentPresenter(IDogFragment view, NetworkService networkService) {
        super(view);
        this.networkService = networkService;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onResumePresenter() {
        networkService.getDogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> view.updatePetsList(response.getPetsList()));
    }
}
