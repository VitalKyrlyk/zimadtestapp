package com.e.zimadtestapp.fragments.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.e.zimadtestapp.R;
import com.e.zimadtestapp.core.baseFragment.BaseFragment;
import com.e.zimadtestapp.utils.Constants;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFragment extends BaseFragment {

    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        ButterKnife.bind(this, view);

        if (getArguments()!= null){
            tvTitle.setText(getArguments().getString(Constants.KEY_TITLE));
            tvNumber.setText(getArguments().getString(Constants.KEY_NUMBER));
            Picasso.with(view.getContext())
                    .load(getArguments().getString(Constants.KEY_URL))
                    .into(imageView);
        }


        return view;
    }
}
