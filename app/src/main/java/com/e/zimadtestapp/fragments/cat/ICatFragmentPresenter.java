package com.e.zimadtestapp.fragments.cat;

import com.e.zimadtestapp.core.baseFragment.IBasePresenterFragment;

public interface ICatFragmentPresenter extends IBasePresenterFragment<ICatFragment> {
}
