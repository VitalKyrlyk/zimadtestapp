package com.e.zimadtestapp.fragments.cat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.e.zimadtestapp.R;
import com.e.zimadtestapp.core.adapter.OnItemClickListener;
import com.e.zimadtestapp.core.adapter.pets.PestAdapter;
import com.e.zimadtestapp.core.adapter.pets.PetsDH;
import com.e.zimadtestapp.core.baseFragment.BaseFragment;
import com.e.zimadtestapp.core.paddingDecoration.PaddingDecoration;
import com.e.zimadtestapp.fragments.detail.DetailFragment;
import com.e.zimadtestapp.network.enteties.Pets;
import com.e.zimadtestapp.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatFragment extends BaseFragment implements ICatFragment, OnItemClickListener {

    @Inject
    ICatFragmentPresenter presenter;

    private View view;
    private PestAdapter adapter;
    private List<Pets> list;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cat, container, false);
        ButterKnife.bind(this, view);

        setUpAdapter();
        if (savedInstanceState != null){
            List<Pets> list = (List<Pets>) savedInstanceState.getSerializable(Constants.KEY_LIST);
            updatePetsList(list);
            recyclerView.scrollToPosition(savedInstanceState.getInt(Constants.KEY_POSITION));
        } else if (getArguments() != null){
            List<Pets> list = (List<Pets>) getArguments().getSerializable(Constants.KEY_LIST);
            updatePetsList(list);
            recyclerView.scrollToPosition(getArguments().getInt(Constants.KEY_POSITION));
        } else {
            presenter.onResumePresenter();
        }

        return view;
    }

    @Override
    public void setUpAdapter(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        adapter = new PestAdapter();
        adapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new PaddingDecoration(view.getContext(), 10));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updatePetsList(List<Pets> list) {
        this.list = list;
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {

                ArrayList<PetsDH> petsDHS = new ArrayList<>();
                for (Pets pets : list) {
                    petsDHS.add(new PetsDH(pets, PestAdapter.PET_TYPE));
                }
                adapter.setListDH(petsDHS);
            });
        }
    }

    @Override
    public void replaceFragment(Fragment fragment, String TAG) {
        FragmentManager manager = getFragmentManager();
        assert manager != null;
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, TAG);
        transaction.commit();
    }

    @Override
    public void onClick(View view, int position, int viewType, Object object) {
        Pets pets = adapter.getListDH().get(position).getPets();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_TITLE, pets.getTitle());
        bundle.putString(Constants.KEY_URL, pets.getUrl());
        bundle.putString(Constants.KEY_NUMBER, String.valueOf(position + 1));

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        replaceFragment(detailFragment, Constants.TAG_DETAIL_FRAGMENT);
    }

    @Override
    public void onPause() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_POSITION, recyclerView.getScrollState());
        bundle.putSerializable(Constants.KEY_LIST, (Serializable) list);
        this.setArguments(bundle);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(Constants.KEY_POSITION, recyclerView.getScrollState());
        outState.putSerializable(Constants.KEY_LIST, (Serializable) list);
        super.onSaveInstanceState(outState);
    }
}
