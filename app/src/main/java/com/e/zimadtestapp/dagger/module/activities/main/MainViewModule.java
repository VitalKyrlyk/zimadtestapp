package com.e.zimadtestapp.dagger.module.activities.main;


import com.e.zimadtestapp.activities.main.IMainActivity;
import com.e.zimadtestapp.activities.main.MainActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainViewModule {

    @Binds
    abstract IMainActivity provideIMainActivity(MainActivity mainActivity);

}
