package com.e.zimadtestapp.dagger.module.fragments.dog;

import com.e.zimadtestapp.fragments.dog.DogFragmentPresenter;
import com.e.zimadtestapp.fragments.dog.IDogFragment;
import com.e.zimadtestapp.fragments.dog.IDogFragmentPresenter;
import com.e.zimadtestapp.network.service.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class DogModule {

    @Provides
    IDogFragmentPresenter provideIDogFragmentPresenter(IDogFragment view, NetworkService networkService){
        return new DogFragmentPresenter(view, networkService);
    }

}
