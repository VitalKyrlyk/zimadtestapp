package com.e.zimadtestapp.dagger.module.activities.main;

import com.e.zimadtestapp.activities.main.IMainActivity;
import com.e.zimadtestapp.activities.main.IMainActivityPresenter;
import com.e.zimadtestapp.activities.main.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class
MainModule {

    @Provides
    IMainActivityPresenter provideIMainActivityPresenter(IMainActivity view){
        return new MainActivityPresenter(view);
    }

}
