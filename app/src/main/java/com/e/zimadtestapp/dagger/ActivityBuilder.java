package com.e.zimadtestapp.dagger;


import com.e.zimadtestapp.activities.main.MainActivity;
import com.e.zimadtestapp.dagger.module.activities.main.MainModule;
import com.e.zimadtestapp.dagger.module.activities.main.MainViewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module()
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainModule.class, MainViewModule.class})
    abstract MainActivity bindMainActivity();

}
