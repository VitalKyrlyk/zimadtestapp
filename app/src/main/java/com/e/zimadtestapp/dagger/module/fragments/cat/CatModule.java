package com.e.zimadtestapp.dagger.module.fragments.cat;

import com.e.zimadtestapp.fragments.cat.CatFragmentPresenter;
import com.e.zimadtestapp.fragments.cat.ICatFragment;
import com.e.zimadtestapp.fragments.cat.ICatFragmentPresenter;
import com.e.zimadtestapp.network.service.NetworkService;

import dagger.Module;
import dagger.Provides;

@Module
public class CatModule {

    @Provides
    ICatFragmentPresenter provideICatFragmentPresenter(ICatFragment view, NetworkService networkService){
        return new  CatFragmentPresenter(view, networkService);
    }

}
