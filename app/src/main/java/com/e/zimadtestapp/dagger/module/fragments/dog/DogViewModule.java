package com.e.zimadtestapp.dagger.module.fragments.dog;

import com.e.zimadtestapp.fragments.dog.DogFragment;
import com.e.zimadtestapp.fragments.dog.IDogFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DogViewModule {

    @Binds
    abstract IDogFragment bindIDogFragment(DogFragment dogFragment);

}
