package com.e.zimadtestapp.dagger.module;

import android.content.Context;
import android.content.SharedPreferences;


import com.e.zimadtestapp.ZimadApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(ZimadApp application) {
        return application.getApplicationContext();
    }

}
