package com.e.zimadtestapp.dagger.components;


import com.e.zimadtestapp.ZimadApp;
import com.e.zimadtestapp.dagger.ActivityBuilder;
import com.e.zimadtestapp.dagger.FragmentBuilder;
import com.e.zimadtestapp.dagger.module.AppModule;
import com.e.zimadtestapp.dagger.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityBuilder.class,
        FragmentBuilder.class })

public interface AppComponent extends AndroidInjector<ZimadApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<ZimadApp> {

    }
}
