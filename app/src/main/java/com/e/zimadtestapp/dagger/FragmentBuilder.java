package com.e.zimadtestapp.dagger;

import com.e.zimadtestapp.dagger.module.fragments.cat.CatModule;
import com.e.zimadtestapp.dagger.module.fragments.cat.CatViewModule;
import com.e.zimadtestapp.dagger.module.fragments.dog.DogModule;
import com.e.zimadtestapp.dagger.module.fragments.dog.DogViewModule;
import com.e.zimadtestapp.fragments.cat.CatFragment;
import com.e.zimadtestapp.fragments.detail.DetailFragment;
import com.e.zimadtestapp.fragments.dog.DogFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module()
public abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = {CatModule.class, CatViewModule.class})
    abstract CatFragment bindCatFragment();

    @ContributesAndroidInjector(modules = {DogModule.class, DogViewModule.class})
    abstract DogFragment bindDogFragment();

    @ContributesAndroidInjector
    abstract DetailFragment bindDetailFragment();
}
