package com.e.zimadtestapp.dagger.module.fragments.cat;

import com.e.zimadtestapp.fragments.cat.CatFragment;
import com.e.zimadtestapp.fragments.cat.ICatFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CatViewModule {

    @Binds
    abstract ICatFragment bindICatFragment(CatFragment catFragment);

}
