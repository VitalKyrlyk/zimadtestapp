package com.e.zimadtestapp.core;

public interface IBasePresenter<V> {

    void onResumePresenter();

}
