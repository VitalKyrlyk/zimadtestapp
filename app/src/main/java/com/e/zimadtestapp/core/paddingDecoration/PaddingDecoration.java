package com.e.zimadtestapp.core.paddingDecoration;

import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PaddingDecoration extends RecyclerView.ItemDecoration {

    private final int mPadding;

    public PaddingDecoration(@NonNull Context context, int padding) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        mPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, padding, metrics);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        final int itemPosition = parent.getChildAdapterPosition(view);
        outRect.left = mPadding;
        outRect.right = mPadding;
        if (itemPosition == RecyclerView.NO_POSITION) {
            return;
        } else if (itemPosition == 0) {
            outRect.bottom = mPadding/2;
        } else {
            outRect.top = mPadding/2;
            outRect.bottom = mPadding/2;
        }
        final RecyclerView.Adapter adapter = parent.getAdapter();
        if ((adapter != null) && (itemPosition == adapter.getItemCount() - 1)) {
            outRect.top = mPadding/2;
        }
    }

}
