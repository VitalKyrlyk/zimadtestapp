package com.e.zimadtestapp.core.adapter.pets;

import com.e.zimadtestapp.core.adapter.RecyclerDH;
import com.e.zimadtestapp.network.enteties.Pets;

public class PetsDH implements RecyclerDH {

    private Pets pets;
    private int type;

    public PetsDH(Pets pets, int type) {
        this.pets = pets;
        this.type = type;
    }


    public Pets getPets() {
        return pets;
    }

    int getGroupType() {
        return type;
    }
}
