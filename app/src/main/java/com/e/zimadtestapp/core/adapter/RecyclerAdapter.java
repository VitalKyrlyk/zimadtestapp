package com.e.zimadtestapp.core.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerAdapter<DH extends RecyclerDH> extends RecyclerView.Adapter<RecyclerVH<DH>> {

    private List<DH> listDH = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    @NonNull
    @Override
    public RecyclerVH<DH> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerVH<DH> holder = createVH(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(viewType), parent, false), viewType);
        if (onItemClickListener != null) {
            holder.setListeners(onItemClickListener);
        }
        return holder;
    }

    @NonNull
    protected abstract RecyclerVH<DH> createVH(View view, int viewType);
    @LayoutRes
    protected abstract int getLayoutRes(int viewType);

    @Override
    public void onBindViewHolder(@NonNull RecyclerVH<DH> holder, int position) {
        holder.bindData(listDH.get(position));
    }

    @Override
    public int getItemCount() {
        return listDH.size();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setListDH(@NonNull List<DH> list) {
        listDH.clear();
        listDH.addAll(list);
        notifyDataSetChanged();
    }

    public List<DH> getListDH() {
        return listDH;
    }


    public void addListDH(List<DH> list) {
        int oldSize = listDH.size();
        listDH.addAll(list);
        notifyItemRangeInserted(oldSize, listDH.size());
    }

    public void updateList() {
        notifyDataSetChanged();
    }

    public DH getItem(int position) {
        if (0 <= position && position < listDH.size()) {
            return listDH.get(position);
        } else {
            return null;
        }
    }

    public void insertItem(DH item, int position) {
        if (0 <= position && position <= listDH.size()) {
            listDH.add(position, item);
            notifyItemInserted(position);
        }
    }

    public void removeItem(int position) {
        if (0 <= position && position < listDH.size()) {
            listDH.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void updateItem(int position) {
        if (0 <= position && position < listDH.size()) {
            notifyItemChanged(position);
        }
    }

    public void changeItem(DH item, int position) {
        if (0 <= position && position < listDH.size()) {
            listDH.set(position, item);
            notifyItemChanged(position);
        }
    }
}
