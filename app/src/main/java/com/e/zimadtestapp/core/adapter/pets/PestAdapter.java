package com.e.zimadtestapp.core.adapter.pets;

import android.view.View;

import androidx.annotation.NonNull;

import com.e.zimadtestapp.R;
import com.e.zimadtestapp.core.adapter.RecyclerAdapter;
import com.e.zimadtestapp.core.adapter.RecyclerVH;


public class PestAdapter extends RecyclerAdapter<PetsDH> {

    public static final int PET_TYPE = 1;

    @NonNull
    @Override
    protected RecyclerVH<PetsDH> createVH(View view, int viewType) {
        if (viewType == PET_TYPE) {
            return new PetsVH(view);
        }
        throw new RuntimeException("PestAdapter :: createVH [Can find such view type]");
    }

    @Override
    protected int getLayoutRes(int viewType) {
        if (viewType == PET_TYPE) {
            return R.layout.item_pet;
        }
        throw new RuntimeException("PestAdapter :: getLayoutRes [Can find such view type]");
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getGroupType();
    }

}
