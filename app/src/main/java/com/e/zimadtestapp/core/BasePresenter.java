package com.e.zimadtestapp.core;

public abstract class BasePresenter<V extends IBaseView> implements IBasePresenter<V> {

    public V view;

    public BasePresenter(V view){
        this.view = view;
    }

}
