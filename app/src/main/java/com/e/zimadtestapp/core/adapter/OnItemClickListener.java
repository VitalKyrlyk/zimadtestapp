package com.e.zimadtestapp.core.adapter;

import android.view.View;

public interface OnItemClickListener {

    void onClick(View view, int position, int viewType, Object object);

}
