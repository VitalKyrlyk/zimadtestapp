package com.e.zimadtestapp.core.baseFragment;


import com.e.zimadtestapp.core.IBasePresenter;

public interface IBasePresenterFragment<V> extends IBasePresenter<V> {

}
