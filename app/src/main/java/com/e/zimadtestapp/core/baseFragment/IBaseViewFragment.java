package com.e.zimadtestapp.core.baseFragment;



import androidx.fragment.app.Fragment;

import com.e.zimadtestapp.core.IBaseView;
import com.e.zimadtestapp.network.enteties.Pets;

import java.util.List;


public interface IBaseViewFragment extends IBaseView {

    void replaceFragment(Fragment fragment, String TAG);

    void setUpAdapter();

    void updatePetsList(List<Pets> list);

}
