package com.e.zimadtestapp.core.adapter.pets;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.e.zimadtestapp.R;
import com.e.zimadtestapp.core.adapter.OnItemClickListener;
import com.e.zimadtestapp.core.adapter.RecyclerVH;
import com.e.zimadtestapp.network.enteties.Pets;
import com.squareup.picasso.Picasso;

public class PetsVH extends RecyclerVH<PetsDH> {

    private View view;
    private ImageView imageView;
    private TextView tvNumber;
    private TextView tvTitle;

    private OnItemClickListener listener;

    PetsVH(View itemView) {
        super(itemView);
        this.view = itemView;
        this.imageView = itemView.findViewById(R.id.image_view);
        this.tvNumber = itemView.findViewById(R.id.tv_item_number);
        this.tvTitle = itemView.findViewById(R.id.tv_title);
    }

    @Override
    public void setListeners(OnItemClickListener listener) {
        this.listener = listener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void bindData(PetsDH data) {
        Pets cats = data.getPets();

        Picasso.with(view.getContext())
                .load(cats.getUrl())
                .into(imageView);
        tvTitle.setText(cats.getTitle());
        tvNumber.setText(String.valueOf(getAdapterPosition() + 1));
        view.setOnClickListener(view -> listener.onClick(view, getAdapterPosition(), getItemViewType(), cats));
    }
}
