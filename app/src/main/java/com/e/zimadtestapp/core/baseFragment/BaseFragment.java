package com.e.zimadtestapp.core.baseFragment;

import android.content.Context;


import com.e.zimadtestapp.activities.main.MainActivity;

import dagger.android.support.DaggerFragment;


public class BaseFragment extends DaggerFragment {

    public MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

}
