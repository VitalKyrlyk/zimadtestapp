package com.e.zimadtestapp.network.enteties;

import com.google.gson.annotations.SerializedName;

public class Pets {

    @SerializedName("url")
    private
    String url;

    @SerializedName("title")
    private
    String title;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
