package com.e.zimadtestapp.network.enteties;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("message")
    private
    String msg;

    @SerializedName("data")
    private
    List<Pets> petsList;

    public String getMsg() {
        return msg;
    }

    public List<Pets> getPetsList() {
        return petsList;
    }
}
