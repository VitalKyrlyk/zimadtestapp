package com.e.zimadtestapp.network.service;

import com.e.zimadtestapp.network.enteties.Response;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface NetworkService {

    @GET("api.php?query=cat")
    Observable<Response> getCats();

    @GET("api.php?query=dog")
    Observable<Response> getDogs();

}
