package com.e.zimadtestapp.activities.main;

import androidx.fragment.app.Fragment;

import com.e.zimadtestapp.core.IBaseView;

public interface IMainActivity extends IBaseView {

    void replaceFragment(Fragment fragment, String TAG);

}
