package com.e.zimadtestapp.activities.main;

import com.e.zimadtestapp.core.BasePresenter;

public class MainActivityPresenter extends BasePresenter<IMainActivity> implements IMainActivityPresenter {


    public MainActivityPresenter(IMainActivity view) {
        super(view);
    }

    @Override
    public void onResumePresenter() {

    }
}
