package com.e.zimadtestapp.activities.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.e.zimadtestapp.R;
import com.e.zimadtestapp.fragments.cat.CatFragment;
import com.e.zimadtestapp.fragments.detail.DetailFragment;
import com.e.zimadtestapp.fragments.dog.DogFragment;
import com.e.zimadtestapp.utils.Constants;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements IMainActivity {

    @Inject
    IMainActivityPresenter presenter;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    private DogFragment dogFragment = new DogFragment();
    private CatFragment catFragment = new CatFragment();
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null){
            catFragment = (CatFragment) getSupportFragmentManager().getFragment(savedInstanceState, Constants.TAG_CAT_FRAGMENT);
            if (getSupportFragmentManager().getFragment(savedInstanceState, Constants.TAG_DOG_FRAGMENT) != null) {
                dogFragment = (DogFragment) getSupportFragmentManager().getFragment(savedInstanceState, Constants.TAG_DOG_FRAGMENT);
            }
            if (savedInstanceState.getString(Constants.TAG_PREVIOUS_FRAGMENT, "").equals(Constants.TAG_DOG_FRAGMENT)){
                Objects.requireNonNull(tabLayout.getTabAt(1)).select();
                currentFragment = dogFragment;
            } else {
                Objects.requireNonNull(tabLayout.getTabAt(0)).select();
                currentFragment = catFragment;
            }
        } else  {
            replaceFragment(catFragment, catFragment.getTag());
        }

        setUpTabLayout();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (currentFragment != fragmentManager.findFragmentById(R.id.fragment_container)) {
            replaceFragment(currentFragment,currentFragment.getTag());
        } else {
            finishAffinity();
        }
    }

    private void setUpTabLayout() {
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    replaceFragment(catFragment, Constants.TAG_CAT_FRAGMENT);
                } else if (tab.getPosition() == 1){
                    replaceFragment(dogFragment, Constants.TAG_DOG_FRAGMENT);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void replaceFragment(Fragment fragment, String TAG) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment tempFragment = manager.findFragmentByTag(TAG);
        if (tempFragment == null){
            if (!(fragment instanceof DetailFragment)) {
                transaction.addToBackStack(TAG);
            }
        }
        transaction.replace(R.id.fragment_container, fragment, TAG);
        transaction.commit();
        currentFragment = fragment;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        getSupportFragmentManager().putFragment(outState, Constants.TAG_CAT_FRAGMENT, catFragment);
        if (getSupportFragmentManager().findFragmentByTag(Constants.TAG_DOG_FRAGMENT) != null) {
            getSupportFragmentManager().putFragment(outState, Constants.TAG_DOG_FRAGMENT, dogFragment);
        }
        outState.putString(Constants.TAG_PREVIOUS_FRAGMENT, currentFragment.getTag());
        super.onSaveInstanceState(outState);
    }

}
